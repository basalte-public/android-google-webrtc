# android-google-webrtc

Google webrtc lib + hardware codecs enabled for Ellie/Lisa

## Summary

Original repo at: https://webrtc.googlesource.com/src.git

Changes include:
* Added Ellie/Lisa prefixes ``OMX.rk.`` ``c2.imx.`` in ``HardwareVideoEncoderFactory.java``;
* Added method `getAllCodecs` for fetching valid codecs in ``HardwareVideoEncoderFactory.java``;

### Build .aar file

```
git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
export PATH=$PATH:/depot_tools
fetch --nohooks webrtc_android
cd src
// Check out desired branch/commit
gclient sync
./build/install-build-deps-android.sh
tools_webrtc/android/build_aar.py
```

# Original README
**WebRTC is a free, open software project** that provides browsers and mobile
applications with Real-Time Communications (RTC) capabilities via simple APIs.
The WebRTC components have been optimized to best serve this purpose.

**Our mission:** To enable rich, high-quality RTC applications to be
developed for the browser, mobile platforms, and IoT devices, and allow them
all to communicate via a common set of protocols.

The WebRTC initiative is a project supported by Google, Mozilla and Opera,
amongst others.

### Development

See [here][native-dev] for instructions on how to get started
developing with the native code.

[Authoritative list](native-api.md) of directories that contain the
native API header files.

### More info

 * Official web site: http://www.webrtc.org
 * Master source code repo: https://webrtc.googlesource.com/src
 * Samples and reference apps: https://github.com/webrtc
 * Mailing list: http://groups.google.com/group/discuss-webrtc
 * Continuous build: https://ci.chromium.org/p/webrtc/g/ci/console
 * [Coding style guide](g3doc/style-guide.md)
 * [Code of conduct](CODE_OF_CONDUCT.md)
 * [Reporting bugs](docs/bug-reporting.md)
 * [Documentation](g3doc/sitemap.md)

[native-dev]: https://webrtc.googlesource.com/src/+/main/docs/native-code/index.md
